#ifndef AWH
#define AWH
#include "types/commrec.h"
/* need to include enums before inputrec because inputrec doesn't include it explicitly though
   it should and want to avoid modifying inputrec */
#include "types/enums.h"
#include "types/inputrec.h"
#include "types/state.h"
#include "../random/random.h"

/* For output in energy file, see gmx_energy.c, mdebin.c */
static const int awh_nvars = 6; /* No. of variables to store in energy file */
static const int nmeta = 3;     /* No of metadata values to store in energy file */

/* TODO: remove */
static const int resettype = 1; /* 0 = reset to Nmin. 1 = reset by trimming.*/

/* Initialize awh method with input data. Allocate memory for awh structure. */
void init_awhbias(FILE *fplog, t_inputrec *ir, t_commrec *cr, t_awhbias *awhbias);

/* Performs awh algorithm for one time step */
void do_awhbias_step(t_awhbias *awhbias, t_pull *pull, t_commrec *cr, double t, gmx_int64_t step);

void prep_awhbias_output(t_awhbias *awhbias, gmx_multisim_t *ms);

gmx_bool prep_awhbias_replica_exchange(t_awhbias *awhbias, int *i, real *gamma);

void update_awhbiashistory(awhbiashistory_t *awhbiashist, t_awhbias *awhbias);

void restore_awhbiashistory_from_state(awhbiashistory_t *awhbiashist, t_awhbias *awhbias,
                                       t_pull *pull, t_commrec *cr);

#endif
