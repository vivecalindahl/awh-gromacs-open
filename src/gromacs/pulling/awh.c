#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "awh.h"
#include "macros.h"
#include "../utility/smalloc.h"
#include "gmx_fatal.h"
#include "physics.h"
#include "network.h"
#include "pull.h"
#include "../fileio/futil.h"
#include "xvgr.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* Static function declarations                                          */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* Move parameter, setting m to new value */
static void move_param(t_awh *awh, t_pull *pull, double *rc, gmx_int64_t step, int seed, gmx_bool bSave_stats);

/* Returns c where e^c = e^a + e^b */
static double expsum(double a, double b);

/* True if int is a power of 2 */
static gmx_bool ispowerof2(int a);

/* Return the 'geometric factor' zeta appearing in Langevin model N0 estimate */
static double zeta(double *xarr, int dim);

/* Set N0 initially according to a rough Langevin-based estimate */
static void set_N0_langevin_estimate(t_awh *awh, double dtlambda);

/* Get sample from discrete probability distribution w on space of size nw */
static void get_sample(double *w, int nw, int *i_sampled,
                       gmx_int64_t step, gmx_int64_t seed, int iseed);

/* Update estimates (update_pmf declared in awh.h in order to call from md.c) */
static void update(t_awh *awh, t_commrec *cr, double t);

/* Update  target distribution and bias function */
static void update_target_and_bias(t_awh *awh);

/* Check covering criterion */
static gmx_bool is_coveredminmax(t_awh *awh, double t);

/* Update N exponentially in initial stage */
static void update_N_initial(t_awh *awh, gmx_bool check_initial, double t);

/* Reset awh, keep current free energy estimates */
static void reset(t_awh *awh, double t);

/* Calculate gamma factor for deconvolution */
static double calc_gamma(t_awh *awh, double *rc);

/* Keep statistics for reaction coordinate */
static void save_rcoord_data(t_awh *awh, t_pull *pull);

/* Get histogram fluctuations */
static double calc_flucts(t_awh *awh);

/* Add data value x to variance of n pts */
static void add_to_variance(t_variance *var, double x);

/*  Update variance */
static void update_variance(t_variance *var);

/* Update average of n pts with value val */
static void update_average(double avg, int npts, double val, double *new_avg);

/* Sets to m the lambda index with lambda point closes to reaction coordinate */
static void get_lambda_index(t_awh *awh, int *m, double *rc);

/* Gets index for  bin for the current reaction coordinate, returns false
   if not found */
static gmx_bool get_rcoord_index(t_awh *awh, int *i, double *rc);

/* Takes a step forward in ndim-dimensional array in one-dimensional layout and
   sets the index for each dimension accordingly */
static void step_in_array(int ndim, int *npoints_dim, int *i_dim);

/* Recursive function wrapped by step_in_array */
static void step_in_array_dimension(int d, int *npoints_dim, int *i_dim);

/* Initialize the lambdapoint grid */
static void init_lambdapoints(t_awh *awh);

/* Initialize the reaction coordinate histogram */
static void init_rcoordbins(t_awh *awh, t_pull *pull);

/* Initialize free energy related awh variables */
static void init_free_energy_vars(t_awh *awh);

/* Initialize awh variables with given values */
static void set_user_vals(t_awh *awh, gmx_bool bPmf, gmx_bool bTarget);

static void init_awh(FILE *fplog, t_inputrec *ir, t_commrec *cr, t_awh *awh);

static gmx_bool do_step(t_awh *awh, t_pull *pull, t_commrec *cr, double t,
                        gmx_int64_t step, int seed, gmx_bool bExchanged);

static void restore_awhhistory_from_state(awhhistory_t *awhhist, t_awh *awh, t_pull *pull, t_commrec *cr);

static void update_awhhistory(awhhistory_t *awhhist, t_awh *awh);

static void update_pmf(t_awh *awh, gmx_multisim_t *ms);

static void update_force_stdev(t_awh *awh);

static void prep_awh_output(t_awh *awh, gmx_multisim_t *ms);

static gmx_bool prep_awh_replica_exchange(t_awh *awh, int *i, real *gamma);



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* Non-static function definitions */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void init_awhbias(FILE *fplog, t_inputrec *ir, t_commrec *cr, t_awhbias *awhbias)
{
    int k, ndim_end;;
    t_awh *awh_end;
    t_pull *pull = ir->pull;

    if (cr != NULL)
    {
        for (k = 0; k < awhbias->nawh; k++)
        {
            awhbias->awh[k].awh_id = k;
            init_awh(fplog, ir, cr, &awhbias->awh[k]);
        }

        /* TODO: ugly, but need to let the other ranks know which are the
           non-awh coords. Remove later. */
        if (MASTER(cr))
        {
            awh_end = &(awhbias->awh[awhbias->nawh - 1]);
            ndim_end = awh_end->ndim;
            awhbias->c_awh = awh_end->icoord[ndim_end - 1]; /* bcast below */
        }
        if (PAR(cr))
        {
            gmx_bcast(1*sizeof(awhbias->c_awh), &(awhbias->c_awh), cr);
            gmx_bcast(pull->ncoord*sizeof(pull->coord[0]), pull->coord, cr);
        }
    }
}

void do_awhbias_step(t_awhbias *awhbias, t_pull *pull, t_commrec *cr, double t, gmx_int64_t step)
{
    int k, c, d;
    t_awh *awh_k;
    gmx_bool bChanged = FALSE;

    for (k = 0; k < awhbias->nawh; k++)
    {
        awh_k = &awhbias->awh[k];
        bChanged |= do_step(awh_k, pull, cr, t, step, awhbias->seed,
                            awh_k->eMultireplica == eawhm_EXCHANGE ? awhbias->bExchanged : FALSE);
    }

    /* AWH in the master rank might have changed the reference coordinates.
       Let other ranks know new values.
       TODO: perhaps a bit of an overkill to bcast the whole coordinate... */
    if (PAR(cr) && bChanged)
    {
        gmx_bcast(pull->ncoord*sizeof(pull->coord[0]), pull->coord, cr);
    }
}

gmx_bool prep_awhbias_replica_exchange(t_awhbias *awhbias, int *i, real *bias_weights)
{
    int k;
    gmx_bool bSuccess;

    k = 0;     /* Currently only supports 1 awh bias per replica */
    bSuccess = prep_awh_replica_exchange(&awhbias->awh[k], i, bias_weights);
    return bSuccess;
}

void prep_awhbias_output(t_awhbias *awhbias, gmx_multisim_t *ms)
{
    int k;

    for (k = 0; k < awhbias->nawh; k++)
    {

        prep_awh_output(&awhbias->awh[k], ms);
    }

}

void update_awhbiashistory(awhbiashistory_t *awhbiashist, t_awhbias *awhbias)
{
    int k;

    awhbiashist->nawhhist = awhbias->nawh;
    if (awhbiashist->awhhist == NULL)
    {
        snew(awhbiashist->awhhist, awhbiashist->nawhhist);
        for (k = 0; k < awhbiashist->nawhhist; k++)
        {
            init_awhhistory(&awhbiashist->awhhist[k]); /* put elsewhere? */
        }
    }
    for (k = 0; k < awhbiashist->nawhhist; k++)
    {
        update_awhhistory(&awhbiashist->awhhist[k], &awhbias->awh[k]);
    }
}


void restore_awhbiashistory_from_state(awhbiashistory_t *awhbiashist, t_awhbias *awhbias,
                                       t_pull *pull, t_commrec *cr)
{
    int k;

    /* Restore the history dependent top level data needed by all ranks */
    awhbias->nawh = awhbiashist->nawhhist;
    if (PAR(cr))
    {
        gmx_bcast(1*sizeof(awhbias->nawh), &(awhbias->nawh), cr); /* TODO: not needed? */
    }

    /* Restore awh data */
    for (k = 0; k < awhbias->nawh; k++)
    {
        restore_awhhistory_from_state(&awhbiashist->awhhist[k], &awhbias->awh[k], pull, cr);
    }

    /* Restore pull history needed by all ranks */
    if (PAR(cr))
    {
        gmx_bcast(pull->ncoord*sizeof(pull->coord[0]), pull->coord, cr);
    }
}



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* Static function definitions */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

static gmx_bool prep_awh_replica_exchange(t_awh *awh, int *ibin, real *bias_weights)
{

    int i;
    gmx_bool bSuccess;

    bSuccess = get_rcoord_index(awh, ibin, awh->rc);

    if (bSuccess)
    {
        /* Get this replica's gamma (~bias) evaluated for all bins */
        for (i = 0; i < awh->nrcoordbin; i++)
        {
            bias_weights[i] = -calc_gamma(awh, awh->rcoordbin[i].rcoord);
        }
    }
    return bSuccess;
}

static void prep_awh_output(t_awh *awh, gmx_multisim_t *ms)
{
    update_pmf(awh, ms);
    update_force_stdev(awh);
    awh->bPrint = TRUE;
}

static void init_awh(FILE *fplog, t_inputrec *ir, t_commrec *cr, t_awh *awh)
{
    const double nsigma = 10.;
    int n, c, d, g, nstupdate;
    double beta, invsqrtbetak;
    t_pull *pull;
    char buf[STRLEN];

    /* Note: these will not be performed when running grompp */
    pull = ir->pull;

    /* Needed by all */
    awh->nxsteps = 0;

    /* Do awh only with master node */
    if(MASTER(cr))
    {
        awh->exp_scale = 2.;
        beta = 1./(BOLTZ*ir->opts.ref_t[0]);

        snew(awh->betak, awh->ndim);
        snew(awh->rc, awh->ndim);
        for(d = 0; d < awh->ndim; d++)
        {
            c = awh->icoord[d];
            awh->betak[d] = beta*pull->coord[c].k;
            awh->rc[d] = pull->coord[c].rc;
        }

        /* Parameter samples are only generated within a box around the current
           reaction coordinate */
        snew(awh->max_dev,awh->ndim);
        for(d = 0; d < awh->ndim; d++)
        {
            invsqrtbetak = awh->betak[d] > 0 ? 1.0/sqrt(awh->betak[d]) : GMX_DOUBLE_MAX;
            awh->max_dev[d] = min(nsigma*invsqrtbetak,GMX_DOUBLE_MAX);
        }

        awh->fplog = fplog;

        /* Multireplica settings */
        if(cr->ms != NULL)
        {
            awh->nreplicas = cr->ms->nsim;
            awh->replica_id = cr->ms->sim;

            /* The current covering criterion implementation does not
               support both multiple sharing replicas and multiple dimensions. */
            if (awh->eMultireplica != eawhm_INDEPENDENT
                && awh->nreplicas > 1 && awh->ndim > 1
                && (awh->edN == eawhnEXP_LINEAR
                    || awh->edN == eawhnEXP_LINEAR_DN
                    || awh->edN == eawhnEXP_LINEAR_NREF))
            {
                gmx_fatal(FARGS,"Both multiple replicas and multiple lambda dimensions"
                          " are currently not supported with"
                          "use of an initial stage covering criterion.");
            }
        }
        else
        {
            awh->nreplicas = 1;
            awh->replica_id = 0;
        }

        awh->nsamples = 0;

        switch (awh->edN)
        {
        case eawhnEXP_LINEAR:
        case eawhnEXP_LINEAR_DN:
        case eawhnEXP_LINEAR_NREF:
            awh->in_initial = TRUE;
            awh->bReset_auto = ir->userint1; /* TODO */
            awh->max_flucts = ir->userreal1; /* TODO */
            break;
        default:
            awh->in_initial = FALSE;
            awh->bReset_auto = FALSE;
        }

        init_lambdapoints(awh);

        if (awh->N0 <= 0)
        {
            /* N0 not specified. Make best attempt to estimate it...x */
            set_N0_langevin_estimate(awh,awh->nstsample*ir->delta_t);
        }

        /* Note: If N0 represents our current error then we could start off by
           an equilibration to a lower error, i.e. perform one initial
           N scaling. But clearer to just take N0 as is. */
        awh->N = awh->N0;
        awh->Nmin = awh->N0;

        /* Reset initial parameter value to the one closest to
           the initial reaction coordinate value */
        get_lambda_index(awh, &(awh->m), awh->rc);

        for(d = 0; d < awh->ndim; d++)
        {
            double lambda_init_d = awh->lambdapoint[awh->m].lambda[d];
            c = awh->icoord[d];

            /* Check that lambda and rcoord are not too far apart initially,
               probably user input error */

            if (fabs(lambda_init_d - awh->rc[d]) > awh->max_dev[d])
            {
                sprintf(buf, "The initial absolute difference between awh%d dim%d lambda (%g nm) "
                        "to coord%d (%g) is larger than %g, %g times the potential width (%g nm). "
                        "This will likely lead to very large forces on your pull groups. "
                        "This can be avoided by increasing the size of your lambda interval(s).",
                        awh->awh_id + 1, d + 1, lambda_init_d, c + 1, awh->rc[d], awh->max_dev[d],
                        nsigma, 1./sqrt(awh->betak[d]));
                gmx_fatal(FARGS, buf);
            }
            pull->coord[c].rc_ref = awh->lambdapoint[awh->m].lambda[d];
        }

        /* Reaction coordinate histogram is prepared automatically based on the lambda grid */
        init_rcoordbins(awh, pull);

        /* Reaction coordinate force */
        snew(awh->force, awh->ndim);
        awh->i_rcoord_prev = -1;
        snew(awh->acceleration, awh->ndim);
        for(d = 0; d < awh->ndim; d++)
        {
            awh->acceleration[d].inv_dt2 = 1./(ir->delta_t*ir->delta_t);
            awh->acceleration[d].nadded = 0;
        }
        awh->reset_force = ir->userint2 != 0;

        /* Initialize pmf, f, target, bias */
        init_free_energy_vars(awh);

        awh->dN_dt = 1./(ir->delta_t*awh->nstsample);

        awh->bPrint = awh->nstout > 0 || awh->bExp_print;

        if(fplog != NULL)
        {
            sprintf(buf, "\nawh%d:", awh->awh_id + 1);
            nstupdate = awh->nstsample*awh->nsamples_update;

            fprintf(fplog,
                    "%s no. of replicas = %d, replica id = %d"
                    "%s no. of lambda points = %d"
                    "%s initial effective sampling time = %g ps "
                    "%s steps/lambda sample = %d (%g ps), "
                    "%s steps/free energy update = %d (%g ps, %d samples)"
                    "%s steps/awh printing = %d (%g ps)",
                    buf, awh->nreplicas, awh->replica_id,
                    buf, awh->nlambda,
                    buf, awh->N0*awh->nstsample*ir->delta_t,
                    buf, awh->nstsample, awh->nstsample*ir->delta_t,
                    buf, nstupdate, nstupdate*ir->delta_t, awh->nsamples_update,
                    buf, awh->nstout, awh->nstout*ir->delta_t);

            for (d = 0; d < awh->ndim; d++)
            {
                fprintf(fplog,"%s dim %d", buf, d + 1);
                fprintf(fplog,
                        "\n       pull coord index = %d"
                        "\n       no. of lambda values = %d"
                        "\n       initial lambda value = %g nm"
                        "\n       min = %g nm, max = %g nm, "
                        "\n       initial reaction coord value = %g nm"
                        "\n       potential width = %g nm\n",
                        awh->icoord[d] + 1,
                        awh->nlambda_dim[d],
                        awh->lambdapoint[awh->m].lambda[d],
                        awh->lambda_min[d], awh->lambda_max[d],
                        awh->rc[d],
                        awh->betak[d] > 0 ? 1.0/sqrt(awh->betak[d]) : GMX_DOUBLE_MAX
                        );
            }
        }
    }
}

static gmx_bool do_step(t_awh *awh, t_pull *pull, t_commrec *cr, double t,
                        gmx_int64_t step, int seed, gmx_bool bExchanged)
{
    int c, d;
    gmx_bool bChanged = FALSE;
    gmx_bool bSave_stats;

   /* Record reaction coordinates */
    if(MASTER(cr) && step > 0)
    {
        save_rcoord_data(awh, pull);
    }

    /* If there was a coordinate exchange, get a new matching lambda
       (could in principle try to swap also the lambda value but that
       become more complicated if the replicas have e.g. different reaction coordinates).
       We don't use this sampling for updating the bias/free energy */
    if (bExchanged)
    {
        if (MASTER(cr))
        {
            bSave_stats = FALSE;
            move_param(awh, pull, awh->rc, step, seed, bSave_stats);
        }

        /* Not only for master to know */
        bChanged = TRUE;
    }

    /* Update PMF (deconvolute) and force variance. Requires summing over sims if multiple */
    if (MASTER(cr) && awh->nstout > 0 && step % awh->nstout == 0)
    {
        prep_awh_output(awh, cr->ms);
    }

    /* Make lambda move if taken enough MD steps */
    if(awh->nxsteps == awh->nstsample)
    {
        if(MASTER(cr))
        {
            /* Generate new lambda sample and save probability weights */
            bSave_stats = TRUE;
            move_param(awh, pull, awh->rc, step, seed, bSave_stats);

            /* Update the free energy estimates and set method
               parameters for the next iteration */
            if(awh->nsamples % awh->nsamples_update == 0)
            {
                update(awh, cr, t);
            }
        }

        /* All ranks need to know about the reference coordinate change */
        bChanged = TRUE;

        /* Reset new lambda sample step count for the next iteration */
        awh->nxsteps = 0;
    }

    awh->nxsteps++;

    return bChanged;
}

static void move_param(t_awh *awh, t_pull *pull, double *rc, gmx_int64_t step, int seed,
                           gmx_bool bSave_stats)
{
    int m, d, c;
    double dev, gamma, *w;
    t_lambdapoint *lpt = awh->lambdapoint;

    /* Prepare discrete Gibbs distribution
       Work with ln w to start with.
       Neglect contribution from points far away from current one. */
    snew(w,awh->nlambda);

    /* e^(-gamma(rc)) = sum_m exp(f_m - Q_m(rc)) */
    gamma = -GMX_DOUBLE_MAX;

    for (m = 0; m < awh->nlambda; m++)
    {
        w[m] =  lpt[m].bias;

        lpt[m].in_scope = TRUE;

        /* Add biasing potential for all parameter dimensions */
        for (d = 0; d < awh->ndim; d++)
        {
            dev = rc[d] - lpt[m].lambda[d];

            /* Only consider jumps within the harmonic potential scope */
            if(fabs(dev) < awh->max_dev[d])
            {
                w[m] -= 0.5*awh->betak[d]*dev*dev;
            }
            else
            {
                lpt[m].in_scope = FALSE;
                break;
            }
        }

        if(lpt[m].in_scope)
        {
            gamma = expsum(gamma, w[m]);
        }
    }

    /* Exponentiate and normalize to 1 */
    for(m = 0; m < awh->nlambda; m++)
    {
        if(lpt[m].in_scope)
        {
            w[m] = exp(w[m] - gamma);
        }
        else
        {
            w[m] = 0.;
        }
    }

    /* Sample new parameter value from w */
    get_sample(w,awh->nlambda, &(awh->m), step, seed, awh->icoord[0]);

    if (bSave_stats)
    {
        awh->nsamples++;

        /* Save weights for next update */
        for(m = 0; m < awh->nlambda; m++)
        {
            lpt[m].W_it += w[m];
        }
    }

    /* Change reference pull coord value to new lambda value */
    for(d = 0; d < awh->ndim; d++)
    {
        c = awh->icoord[d];
        pull->coord[c].rc_ref = awh->lambdapoint[awh->m].lambda[d];
    }

    sfree(w);
}

static double expsum(double a, double b)
{
    return (a > b ? a : b) + log1p(exp(-fabs(a - b)));
}

static gmx_bool ispowerof2(int a)
{
    /* Ignore a = 0 case */
    return (a & (a - 1)) == 0;
}

static double zeta(double *xarr, int dim)
{
    /* Geometric factor in N0 estimate, x = sigma/L */
    const double xs[] =
        {1e-5, 1e-4, 1e-3, 1e-2, 1e-1};
    const double zeta_1d[] =
        {0.166536811948, 0.16653116886, 0.166250075882,
         0.162701098306, 0.129272430287};
    const double zeta_2d[] =
        {2.31985974274, 1.86307292523, 1.38159772648,
         0.897554759158, 0.405578211115};
    const double *zs;
    int d, i, npts = asize(xs);
    double w, x, x0, x1, z;

    if (dim == 1)
    {
        zs = zeta_1d;
    }
    else if (dim == 2)
    {
        zs = zeta_2d;
    }
    else
    {
        /* TODO... */
        zs = zeta_2d;
    }

    /* TODO: geometric average, but really should have d-dimensional look-up table */
    x = 1.;
    for (d = 0; d < dim; d++)
    {
        x *= xarr[d];
    }

    x = pow(x, 1./dim);

    /* Look up zeta(x) */
    i = 0;
    while (i < npts && x > xs[i])
    {
        i++;
    }

    if (i == npts)
    {
        i = npts - 1;
        /* Take last value */
        z = zs[i];
    }
    else if (i == 0)
    {
        z = zs[i];
    }
    else
    {
        /* Interpolate */
        x0 = xs[i - 1];
        x1 = xs[i];
        w = (x - x0)/(x1 - x0);
        z = w*zs[i - 1] + (1 - w)*zs[i];
    }
    return z;
}

static void set_N0_langevin_estimate(t_awh *awh, double dtlambda)
{
    int d;
    double L, s, error02, L2invD;
    double *x;

    snew(x, awh->ndim);

    /* Get diffusion factor */
    L2invD = 0.;
    for (d = 0; d < awh->ndim; d++)
    {
        L = awh->lambda_max[d] - awh->lambda_min[d];
        L2invD += awh->diffusion[d]/(L*L);
        s = 1./sqrt(awh->betak[d]);
        x[d] = s/L;
    }
    L2invD = 1./L2invD;

    /* Take note of diffusion time */
    if (awh->fplog != NULL)
    {
        char buf[STRLEN];
        sprintf(buf, "\nawh%d:", awh->awh_id + 1);
        fprintf(awh->fplog, "%s estimated interval diffusion time = %g ps ",
                buf, 0.5*L2invD);
        fprintf(awh->fplog, "(using diffusion constants: ");
        for (d = 0; d < awh->ndim; d++)
        {
            fprintf(awh->fplog, "%g ", awh->diffusion[d]);
        }
        fprintf(awh->fplog, "nm^2/ps)");
    }

    error02 = awh->error0*awh->error0;

    awh->N0 = round(L2invD*zeta(x, awh->ndim)/(error02*dtlambda));
    sfree(x);
}

static void get_sample(double *w, int nw, int *i_sampled, gmx_int64_t step, gmx_int64_t seed, int iseed)
{
    int i, imin, imax, imid;
    gmx_bool found;
    double rc, rnd[2];
    double *w_cum;

    snew(w_cum,nw);

    w_cum[0] = w[0];

    for(i = 1; i < nw; i++)
    {
        w_cum[i] = w_cum[i-1] + w[i];
    }

    /* Use binary search to convert rcoord to an integer in [0,nw-1] distributed according to w */
    gmx_rng_cycle_2uniform(step, iseed, seed, RND_SEED_AWH, rnd);
    rc = rnd[0];

    imin = 0; /* min index */
    imax = nw - 1; /* max index */
    found = FALSE;
    do
    {
        /* start with (lower) middle element */
        imid = (imin+imax)/2;

        /* search in upper then lower array half */
        if (rc >= w_cum[imid])
        {
            imin = imid + 1;
        }
        else if( imid > 0 && rc <  w_cum[imid-1])
        {
            imax = imid - 1;
        }
        else
        {
            found = TRUE;
        }

        /* only one element left */
        if (imin == imax)
        {
            found = TRUE;
            imid = imax;
        }
    } while(!found);

    *i_sampled = imid;

    sfree(w_cum);
}

/* TODO: clean and reorganize updates? */
static void update(t_awh *awh, t_commrec *cr, double t)
{
    int i, m, nreplicas, check_freq;
    double dN, N_old, W, W_target, histo_scale, df, sum_df, flucts;
    double *all_W_it;
    gmx_int64_t *all_visits_it;
    gmx_int64_t nsamples_tot;
    t_lambdapoint *lpt = awh->lambdapoint;
    t_rcoordbin *rcbin = awh->rcoordbin;
    gmx_multisim_t *ms = cr->ms;

    check_freq = awh->nsamples_update == 1 ? 100 : awh->nsamples_update;

    /* Add up weights for all simulations */
    snew(all_W_it,awh->nlambda);
    snew(all_visits_it,awh->nlambda);

    for(m = 0; m < awh->nlambda; m++)
    {
        all_W_it[m] = lpt[m].W_it;
        all_visits_it[m] = rcbin[m].visits_it;
    }

    if(awh->nreplicas > 1 && awh->eMultireplica == eawhm_SHARE)
    {
        gmx_sumd_sim(awh->nlambda,all_W_it,ms);
        gmx_sumli_sim(awh->nlambda,all_visits_it,ms);
    }

    /* Add to weight histogram */
    dN = 0.;

    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].W_cover += all_W_it[m];
        lpt[m].W_tot += all_W_it[m];
        rcbin[m].visits_tot += all_visits_it[m];
        dN += all_W_it[m];
    }

    N_old = awh->N;

    /* Monitor fluctuations in weight histogram */
    flucts = calc_flucts(awh);
    if (awh->in_initial && awh->Nmin <= awh->N0) /* TODO nicer: only update flucts in first "initial" stage */
    {
        /* Get initial estimate of what flucts could be expected */
        update_average(awh->flucts_initial, awh->nflucts_initial,
                       flucts, &(awh->flucts_initial));
    }

    /* Update free energy estimate */
    sum_df = 0.;
    for(m = 0; m < awh->nlambda; m++)
    {
        W = N_old * lpt[m].target + all_W_it[m];
        W_target = N_old * lpt[m].target + dN*lpt[m].target;
        df = -log(W/W_target);
        sum_df += df;
        lpt[m].f += df;
    }

    /* Update N */
    switch (awh->edN)
    {
    case eawhnNONE:
        break;
    case eawhnLINEAR:
        if ( awh->bExp_print &&
             ((int)round(awh->N) % (int)round(awh->N0)) == 0 && ispowerof2((int)round(awh->N/awh->N0)) )
        {
            prep_awh_output(awh, cr->ms);
        }

        awh->N += dN;
        break;
    case eawhnEXP_LINEAR:
        if (awh->in_initial)
        {
            /* If covered enough, update N */
            if ((awh->nsamples % check_freq == 0) && is_coveredminmax(awh, t))
            {
                update_N_initial(awh, TRUE, t);
                if (awh->bExp_print)
                {
                    prep_awh_output(awh, cr->ms);
                }
            }
            /* Else keep N const */
        }
        else
        {
            if ( awh->bExp_print &&
                 ((int)round(awh->N) % (int)round(awh->N0)) == 0 && ispowerof2((int)round(awh->N/awh->N0)) )
            {
                prep_awh_output(awh, cr->ms);
            }

            awh->N += dN;
        }

        break;
    case eawhnEXP_LINEAR_NREF:
        if (awh->in_initial)
        {
            /* If covered enough, update N */
            if ((awh->nsamples % check_freq == 0) && is_coveredminmax(awh, t))
            {
                update_N_initial(awh, TRUE, t);
                if (awh->bExp_print)
                {
                    prep_awh_output(awh, cr->ms);
                }
            }
            /* Else keep N const */
        }
        else
        {
            /* If covered enough, update N to N = Nref */
            if ((awh->nsamples % check_freq == 0) && is_coveredminmax(awh, t))
            {
                update_N_initial(awh, FALSE, t);
                if (awh->bExp_print)
                {
                    prep_awh_output(awh, cr->ms);
                }
                if ( awh->bExp_print &&
                     ((int)round(awh->N) % (int)round(awh->N0)) == 0 && ispowerof2((int)round(awh->N/awh->N0)) )
                {
                    prep_awh_output(awh, cr->ms);
                }
            }
            /* Else keep N const */
        }

        break;
    case eawhnEXP:
        if ((awh->nsamples % check_freq == 0) && is_coveredminmax(awh, t))
        {
            update_N_initial(awh, FALSE, t);
            if (awh->bExp_print)
            {
                prep_awh_output(awh, cr->ms);
            }
        }
        break;
    case eawhnEXP_LINEAR_DN:
        if (awh->in_initial)
        {
            /* If covered enough, update N */
            if ((awh->nsamples % check_freq == 0) && is_coveredminmax(awh, t))
            {
                update_N_initial(awh, TRUE, t);
                if (awh->bExp_print)
                {
                    prep_awh_output(awh, cr->ms);
                }
            }
            /* Else keep N const */
        }
        else
        {
            if ( awh->bExp_print &&
                 ((int)round(awh->N) % (int)round(awh->N0)) == 0 && ispowerof2((int)round(awh->N/awh->N0)) )
            {
                prep_awh_output(awh, cr->ms);
            }

            awh->N += dN;
        }

        break;
    }

    /* Update target distribution and biasing weights g */
    update_target_and_bias(awh);

    /* (Potentially trim histogram here if it is too skewed.
       Should then take care of the normalization of f, eg. reset to 0. Otherwise if reset
       several times it might grow very large) */
    if (!awh->in_initial && awh->bReset_auto && (flucts/awh->flucts_initial > awh->max_flucts))
    {
        /* Restart from last Nmin */
        reset(awh, t);
        /* TODO: if reset repeatedly, might be problems.
           With F cutoff: cutoff and N0 not too small? Otherwise, reaction coordinate issues?
        */
    }

    /* Scale the accumulated sum exp(gamma) by Nnew/(Nold+dN) (= 1 for "natural" growth) */
    histo_scale = log(awh->N/(N_old + dN));

    for (i = 0; i < awh->nrcoordbin; i++)
    {
        /* a exp(b) = exp(lna + b) */
        awh->rcoordbin[i].gammas += histo_scale;
    }

    /* Reset for next iteration */
    /* Renormalize f (doesn't affect update but norm may accumulate e.g. if N is kept constant) */
    sum_df /= awh->nlambda;

    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].W_it = 0.;
        rcbin[m].visits_it = 0;
        lpt[m].f -= sum_df;
    }

    sfree(all_W_it);
    sfree(all_visits_it);
}

static gmx_bool is_coveredminmax(t_awh *awh, double t)
{
    int d, m;
    t_lambdapoint *lpt = awh->lambdapoint;
    gmx_bool covered;
    double f_max, Wpeak;

    /* Get lambda min and max cutoff and max f if needed */
    f_max = GMX_DOUBLE_MAX;

    if (awh->etarget == eawhtCUTOFF || awh->etarget == eawhtUSER_CUTOFF)
    {
        /* Find max  frelative to global min */
        double f_diff_max = awh->target_param;
        double f_min = GMX_DOUBLE_MAX;
        for(m = 0; m < awh->nlambda; m++)
        {
            if(lpt[m].f < f_min)
            {
                f_min = lpt[m].f;
            }
        }

        f_max = f_min + f_diff_max;

        /* Note: the below check only guarantees that parameter space is
           covered for the case of 1 replica. For multiple replicas and multiple dimensions
           it could very well happen that the boundary lines haven been visited but that
           the replicas are isolated such that there is no connected path
           spanning the space (TODO: Djikstra's algorithm or similar) */

        /* Find max, min lambda with f < f_max in each dimension */
        for (d = 0; d < awh->ndim; d++)
        {
            awh->lambda_min_cutoff[d] = GMX_DOUBLE_MAX;
            awh->lambda_max_cutoff[d] = -GMX_DOUBLE_MAX;
        }

        for (d = 0; d < awh->ndim; d++)
        {
            for(m = 0; m < awh->nlambda; m++)
            {
                if (lpt[m].f < f_max &&
                    lpt[m].lambda[d] < awh->lambda_min_cutoff[d])
                {
                    awh->lambda_min_cutoff[d] = lpt[m].lambda[d];
                }
                if (lpt[m].f < f_max &&
                    lpt[m].lambda[d] > awh->lambda_max_cutoff[d])
                {
                    awh->lambda_max_cutoff[d] = lpt[m].lambda[d];
                }
                if(awh->lambda_min_cutoff[d] == awh->lambda_min[d] &&
                   awh->lambda_max_cutoff[d] == awh->lambda_max[d])
                {
                    break;
                }
            }
        }
    }

    /* Covering cutoff: Wc = Wpeak
       Wpeak = weight at peak from mean assuming Gaussian transition prob. distr. */
    /* TODO: should use point index to check if boundary point instead of floating point lambda value */
    Wpeak = 1;
    for (d = 0; d < awh->ndim; d++)
    {
        Wpeak *= awh->dlambda[d]*sqrt(awh->betak[d]*0.5*M_1_PI);
    }

    /* non-independent replicas: ndim = 1 (ndim > 1 N/A) */
    if(awh->nreplicas > 1 && awh->eMultireplica != eawhm_INDEPENDENT )
    {
        /* Knowing connectedness of paths would need extra work.
           For 1D we can simply check each point.*/
        covered = TRUE;
        d = 0;
        m = 0;

        /* skip to first point within interval */
        while (lpt[m].lambda[d] < awh->lambda_min_cutoff[d])
        {
            m++;
        }

        /* Check if any points within the remaining interval fails the criterion */
        while (m < awh->nlambda && lpt[m].lambda[d] <= awh->lambda_max_cutoff[d])
        {
            if (lpt[m].W_cover < Wpeak)
            {
                covered = FALSE;
                break;
            }
            m++;
        }
    }
    else /* independent replicas: ndim >= 1 */
    {
        /* for each dimension find a point with lambda <= min cutoff
           and a point with lambda >= max cutoff which both have  W >= Wpeak */
        covered = FALSE;
        for (d = 0; d < awh->ndim; d++)
        {
            /* Check min condition for this dim */
            covered = FALSE;
            for (m = 0; m < awh->nlambda; m++)
            {
                if (lpt[m].lambda[d] <= awh->lambda_min_cutoff[d] &&
                    lpt[m].f < f_max &&
                    lpt[m].W_cover >= Wpeak)
                {
                    covered = TRUE;
                    break;
                }
            }
            if (!covered)
            {
                break;
            }

            /* Check max condition for this dim */
            covered = FALSE;
            for (m = 0; m < awh->nlambda; m++)
            {
                if (lpt[m].lambda[d] >= awh->lambda_max_cutoff[d] &&
                    lpt[m].f < f_max &&
                    lpt[m].W_cover >= Wpeak)
                {
                    covered = TRUE;
                    break;
                }
            }
            if (!covered)
            {
                break;
            }
        }
    }

    if (covered && awh->fplog != NULL)
    {
        char buf[STRLEN];
        sprintf(buf, "\nawh%d:", awh->awh_id + 1);
        fprintf(awh->fplog,"%s covering at t = %g ps\n", buf, t);
        for (d = 0; d < awh->ndim; d++)
        {
            fprintf(awh->fplog," dim %d: interval [%g, %g] nm\t",
                    d + 1, awh->lambda_min_cutoff[d], awh->lambda_max_cutoff[d]);
        }
    }

    return covered;
}

static void update_N_initial(t_awh *awh, gmx_bool check_initial, double t)
{
    int i, d, m;
    t_lambdapoint *lpt = awh->lambdapoint;
    double N_old;
    double nsamples_tot;
    double dN;
    gmx_bool exit;
    char buf[STRLEN];

    sprintf(buf, "\nawh%d:", awh->awh_id + 1);

    dN = 0.;
    nsamples_tot = 0.;
    for (m = 0; m < awh->nlambda; m++)
    {
        dN += lpt[m].W_cover;
        lpt[m].W_cover = 0.;
        nsamples_tot += lpt[m].W_tot;
    }

    N_old = awh->N;

    if (awh->edN == eawhnEXP_LINEAR_NREF && awh->in_initial == FALSE)
    {
        double Nref = awh->N0 + nsamples_tot;
        awh->exp_scale = Nref/awh->N; /* => Nnew = N*Nref/N = Nref */
    }

    awh->N *= awh->exp_scale;

    /* Check if out of initial stage and reset N if so */
    exit = FALSE;
    if (check_initial)
    {
        double Nref;

        if (awh->edN == eawhnEXP_LINEAR_DN)
        {
            double dt, dN_dt_initial;
            Nref = N_old + dN;
            dt = dN/awh->dN_dt;
            dN_dt_initial = (awh->exp_scale-1.)*N_old/dt;
            exit = dN_dt_initial >= awh->dN_dt;
        }
        else
        {
            if (resettype == 0)
            {
                Nref = awh->Nmin + nsamples_tot;
            }
            else
            {
                Nref =  awh->N0 + nsamples_tot;
            }
            exit = awh->N >= Nref;
        }
        if (exit)
        {
            awh->Nmin = N_old; /* After first exit Nmin >= N0 */
            awh->N = Nref;
            awh->in_initial = FALSE;

            if(awh->reset_force)
            {
                t_rcoordbin *rcbin = awh->rcoordbin;
                for (i = 0; i < awh->nrcoordbin; i++)
                {
                    rcbin[i].force_stdev = 0;
                    for (d = 0; d < awh->ndim; d++)
                    {
                        rcbin[i].force_variance[d].avg_x = 0;
                        rcbin[i].force_variance[d].avg_x2 = 0;
                        rcbin[i].force_variance[d].var_x = 0;
                        rcbin[i].force_variance[d].nx = 0;
                    }
                }
            }
        }
    }

    if(awh->fplog != NULL)
    {
        fprintf(awh->fplog,"%s increased effective sampling time to %g ps\n",
                buf, awh->N/awh->dN_dt);
        if (check_initial && exit)
        {
            fprintf(awh->fplog,"%s out of initial stage at t = %g ps\n",
                    buf, t);
        }
    }

}

static void reset(t_awh *awh, double t)
{
    int m;
    t_lambdapoint *lpt = awh->lambdapoint;
    t_rcoordbin *rcbin = awh->rcoordbin;
    char buf[STRLEN];

    sprintf(buf, "\nawh%d:", awh->awh_id + 1);

    if (resettype == 0) /* Reset to last "initial stage" N */
    {
        /* Reset histograms */
        /* NOTE: should not have to reset awh->nsamples since this
           variable should only be used as a counter */
        for (m = 0; m < awh->nlambda; m++)
        {
            lpt[m].W_it = 0.;
            lpt[m].W_cover = 0.;
            lpt[m].W_tot= 0.; /* NOTE: affects Nref = Nmin + nsamples_tot */

            /* For consistency do the same for reaction coord */
            rcbin[m].visits_it = 0;
            rcbin[m].visits_tot = 0;
        }
        /* These estimates should keep on getting better, don't reset.
           awh->flucts_initial = 0.;
           awh->nflucts_initial = 0; */
        /* Restart in initial stage mode where applicable */
        switch (awh->edN)
        {
        case eawhnEXP_LINEAR:
        case eawhnEXP_LINEAR_DN:
        case eawhnEXP_LINEAR_NREF:
            awh->in_initial = TRUE;
            break;
        default:
            awh->in_initial = FALSE;
        }

        /* Reset N to Nmin */
        if(awh->fplog != NULL)
        {
            fprintf(awh->fplog,"%s resetting at t = %g ps; "
                    "effective sampling time = %g ps --> %g ps\n",
                    buf, t, awh->N/awh->dN_dt, awh->Nmin/awh->dN_dt);
        }
        awh->N = awh->Nmin;
    }
    else if (resettype == 1) /* Reset by trimming */
    {
        double nsamples_tot = 0., nsamples_new = 0.;

        for (m = 0; m < awh->nlambda; m++)
        {
            nsamples_tot += lpt[m].W_tot;
        }

        /* Histogram resetting */
        /* TODO: make sure compatible with f cutoff */
        for (m = 0; m < awh->nlambda; m++)
        {
            lpt[m].W_it = 0.;
            lpt[m].W_cover = 0.;

            lpt[m].W_tot = min(lpt[m].W_tot, nsamples_tot*lpt[m].target);
            nsamples_new += lpt[m].W_tot;

            /* For consistency do the same for reaction coord */
            rcbin[m].visits_it = 0;
            /* rcbin[m].visits_tot = 0; TODO? */
        }

        /* Restart in initial stage mode where applicable */
        switch (awh->edN)
        {
        case eawhnEXP_LINEAR:
        case eawhnEXP_LINEAR_DN:
        case eawhnEXP_LINEAR_NREF:
            awh->in_initial = TRUE;
            break;
        default:
            awh->in_initial = FALSE;
        }
        {
            fprintf(awh->fplog,"%s resetting at t = %g ps; "
                    "effective sampling time = %g ps --> %g ps\n",
                    buf, t, awh->N/awh->dN_dt, (awh->N0 + nsamples_new)/awh->dN_dt);
        }
        //        awh->N = awh->Nmin + nsamples_new; /* Nmin = N0 */
        awh->N = awh->N0 + nsamples_new;  // TEST
    }

    /* Renormalizing the free energies should anyway be done outside this fcn */
}

static double calc_gamma(t_awh *awh, double *rc)
{
    int m,d;
    double dev, gamma, w;
    t_lambdapoint *lpt = awh->lambdapoint;
    gmx_bool in_scope;

    gamma = -GMX_DOUBLE_MAX;

    for (m = 0; m < awh->nlambda; m++)
    {
        w =  lpt[m].bias;

        in_scope = TRUE;

        /* Add biasing potential for all parameter dimensions */
        for (d = 0; d < awh->ndim; d++)
        {
            dev = rc[d] - lpt[m].lambda[d];

            /* Only consider jumps within the harmonic potential scope */
            if(fabs(dev) < awh->max_dev[d])
            {
                w -= 0.5*awh->betak[d]*dev*dev;
            }
            else
            {
                in_scope = FALSE;
                break;
            }
        }

        if(in_scope)
        {
            gamma = expsum(gamma, w);
        }
    }

    /* e^gamma(r) = 1/e^(-gamma(r)) */
    return -gamma;
}

static void save_rcoord_data(t_awh *awh, t_pull *pull)
{
    int c, d, i_rcoord;
    t_rcoordbin *rcbin;
    gmx_bool bHave_history  = FALSE;
    t_acceleration *acc;

    /* Keep own copy of current reaction coordinate value */
    for (d = 0; d < awh->ndim; d++)
    {
        c = awh->icoord[d];
        awh->rc[d] = pull->coord[c].rc;
    }

    /* Calc and save gamma and reaction coordinate histogram */
    if(get_rcoord_index(awh, &i_rcoord, awh->rc))
    {
        rcbin = &awh->rcoordbin[i_rcoord];
        rcbin->gammas = expsum(rcbin->gammas, calc_gamma(awh,awh->rc));
        rcbin->visits_it += 1;
    }

    /* Force statistics */
    /* Shift saved data to give room for new */
    for (d = 0; d < awh->ndim; d++)
    {
        acc = &(awh->acceleration[d]);
        acc->x2 = acc->x1;               /* save 2 steps back */
        acc->x1 = acc->x;                /* save 1 step back */
        acc->x = awh->rc[d];               /* add current */
        acc->nadded++;
    }

    bHave_history = acc->nadded >= 3;
    if (bHave_history)
    {
        /* Have added all previous data needed for acceleration and force of previous step */
        for (d = 0; d < awh->ndim; d++)
        {
            acc->a1 = (acc->x - 2*acc->x1 + acc->x2)*acc->inv_dt2;
        }
    }

    if (awh->i_rcoord_prev >= 0 && bHave_history)
    {
        /* Get the reaction coordinate force f = m*a - f_umbrella */
        for (d = 0; d < awh->ndim; d++)
        {
            rcbin = &awh->rcoordbin[awh->i_rcoord_prev];
            awh->force[d] += pull->coord[c].m_reduced*acc->a1; /* add acceleration force term */
            add_to_variance(&rcbin->force_variance[d], awh->force[d]);
        }
    }

    /* Really only need to do update force variance next step if the reaction coordinate
       is  within range (of a bin) */
    awh->i_rcoord_prev = i_rcoord;
    if (awh->i_rcoord_prev >= 0)
    {
        /* Add the umbrella force term for this step to use at next step */
        for (d = 0; d < awh->ndim; d++)
        {
            awh->force[d] = pull->coord[c].k*(awh->rc[d] - awh->lambdapoint[awh->m].lambda[d]);
        }
    }
}

static double calc_flucts(t_awh *awh)
{
    int m;
    double inv_norm = 0., dev, flucts = 0., f_max = GMX_DOUBLE_MAX;
    t_lambdapoint *lpt = awh->lambdapoint;

    /* TODO: possibly have f_min as a awh variable...? */
    if (awh->etarget == eawhtCUTOFF || awh->etarget == eawhtUSER_CUTOFF)
    {
        /* Find max  frelative to global min */
        double f_diff_max = awh->target_param;
        double f_min = GMX_DOUBLE_MAX;
        for(m = 0; m < awh->nlambda; m++)
        {
            if(lpt[m].f < f_min)
            {
                f_min = lpt[m].f;
            }
        }

        f_max = f_min + f_diff_max;
    }

    for (m = 0; m < awh->nlambda; m++)
    {
        if (lpt[m].f < f_max)
        {
            inv_norm += lpt[m].W_tot;
        }
    }
    inv_norm = 1./inv_norm;

    for (m = 0; m < awh->nlambda; m++)
    {
        if (lpt[m].f < f_max)
        {
            dev = lpt[m].W_tot*inv_norm - lpt[m].target;
            flucts += dev*dev;
        }
    }

    flucts *= awh->N;
    return flucts;
}

static void add_to_variance(t_variance *var, double x)
{
    update_average(var->avg_x, var->nx, x, &(var->avg_x));
    update_average(var->avg_x2, var->nx, x*x, &(var->avg_x2));
    var->nx++;

    update_variance(var);
}

static void update_variance(t_variance *var)
{
    var->var_x = var->avg_x2 - var->avg_x*var->avg_x; /* var x = <x^2> - <x>^2 */
}

static void update_average(double avg, int npts, double val, double *new_avg)
{
    double inv_np1 = 1./(npts + 1);
    *new_avg = npts*inv_np1*avg + val*inv_np1;
}

static void get_lambda_index(t_awh *awh, int *m, double *rc)
{
    double m_dim;
    int d,dim_offset;

    /* One-dimensional indexing of multidimensional array:
       A[i1,i2.., indim] = A[Nndim*...*N3*N2*i1 + Nndim*...*N3*i2 + ... + indim],
       dim_offset_i= Nndim*...N(i+1) */
    dim_offset = 1;
    (*m) = 0;

    for(d = awh->ndim - 1; d >= 0; d--)
    {
        if (awh->dlambda[d] > 0)
        {
            m_dim = (rc[d] - awh->lambdapoint[0].lambda[d])/awh->dlambda[d] + 0.5;
        }
        else
        {
            m_dim = 0;
        }

        dim_offset *= (d < (awh->ndim - 1) ? awh->nlambda_dim[d + 1] : 1);

        if ( (m_dim >= 0) && (m_dim < (awh->nlambda_dim[d])) )
        {
            (*m) += dim_offset * (int)m_dim;
        }
        else
        {
            /* If outside of lambda grid, choose closest point on edge. */
            (*m) += dim_offset * (m_dim < 0 ? 0 : (awh->nlambda_dim[d] - 1));
        }
    }
}

static gmx_bool get_rcoord_index(t_awh *awh, int *i, double *rc)
{
    int dim_offset, d;
    double i_dim;
    gmx_bool bSuccess;

   /* One-dimensional indexing of multidimensional array:
       A[i1,i2.., indim] = A[Nndim*...*N3*N2*i1 + Nndim*...*N3*i2 + ... + indim],
       dim_offset_i = Nndim*...N(i+1)
   */

    bSuccess = TRUE;
    dim_offset = 1;
    (*i) = 0;
    for(d = awh->ndim - 1; d >= 0; d--)
    {
        if (awh->drcoord[d] > 0)
        {
            i_dim = (rc[d] - awh->rcoordbin[0].rcoord[d])/awh->drcoord[d] + 0.5;
        }
        else
        {
            i_dim = 0;
        }

        dim_offset *= (d < awh->ndim - 1? awh->nrcoordbin_dim[d + 1] : 1);

        if ( (i_dim >= 0) && (i_dim < (awh->nrcoordbin_dim[d])) )
        {
            (*i) += dim_offset * (int)i_dim;
        }
        else
        {
            /* Fell outside of all bins */
            bSuccess = FALSE;
            (*i) = -1;
            break;
        }
    }

    return bSuccess;
}

static void step_in_array(int ndim, int *npoints, int *i)
{
    step_in_array_dimension(ndim - 1, npoints, i);
}

static void step_in_array_dimension(int d, int *npoints, int *i)
{
    i[d]++;

    if(i[d] == npoints[d])
    {
        /* Stepped out of array bounds in this dimension, reset index */
        i[d] = 0;

        /* Recursively check any lower dimensions */
        if(d > 0)
        {
            d--;
            step_in_array_dimension(d,npoints,i);
        }
    }
}

static void init_lambdapoints(t_awh *awh)
{
    const double pointdensity = 3;
    int i, d, m, n, c, dim_index;
    int *mm, *m_temp;
    double invsqrtbetak, nsigma;
    double *lambda_temp;
    t_lambdapoint *lpt;
    char buf[STRLEN];

    sprintf(buf, "\nawh%d:", awh->awh_id + 1);

    /* Set the dimensions of lambda grid */
    awh->nlambda = 1;

    for(d = 0; d < awh->ndim; d++)
    {
        if(awh->nlambda_dim[d] == 0)
        {
            /* No user given value. Determine one using a constant value of
               points per 1/sqrt(beta*k) (do in read_awh_params instead?).*/
            if(awh->lambda_max[d] - awh->lambda_min[d] == 0)
            {
                awh->nlambda_dim[d] = 1;
            }
            else if(awh->betak[d] == 0)
            {
                awh->nlambda_dim[d] = 2;
            }
            else
            {
                nsigma = (awh->lambda_max[d] - awh->lambda_min[d])*
                    sqrt(awh->betak[d]);
                awh->nlambda_dim[d] = max(2, (int)ceil(pointdensity * nsigma));
            }
        }
        awh->nlambda *= awh->nlambda_dim[d];
    }

    snew(awh->lambdapoint,awh->nlambda);
    snew(awh->dlambda,awh->ndim);

    lpt = awh->lambdapoint;

    /* Allocate all lambda point coordinates and indices in one chunk */
    snew(lambda_temp,awh->nlambda*awh->ndim);
    dim_index = 0;

    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].lambda = &(lambda_temp[dim_index]);
        dim_index += awh->ndim;
    }

    lambda_temp = NULL;

    snew(m_temp,awh->nlambda*awh->ndim);
    dim_index = 0;

    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].m = &(m_temp[dim_index]);
        dim_index += awh->ndim;
    }

    m_temp = NULL;

    for(d = 0; d < awh->ndim; d++)
    {

        awh->dlambda[d] = awh->nlambda_dim[d] > 1 ?
            (awh->lambda_max[d] - awh->lambda_min[d])/(awh->nlambda_dim[d] - 1) : 0;
    }

    /* Check that dLambda < sqrt(1/(k*beta)) for efficient jumps in lambda */
    /* This check could be removed if the point density is automatized or parametrized */
    for(d = 0; d < awh->ndim; d++)
    {
        invsqrtbetak = awh->betak[d] > 0 ? 1.0/sqrt(awh->betak[d]) : GMX_DOUBLE_MAX;

        if(awh->dlambda[d] > invsqrtbetak)
        {
            fprintf(stderr,"%s dlambda (%g) > stdev (%g) for dimension %d",
                    buf, awh->dlambda[d], invsqrtbetak, d);
        }
    }

    /* Help array keeping track of what lambda index we are at in each dimension */
    snew(mm,awh->ndim);

    /* Start at (0,0,...) */
    for(d = 0; d < awh->ndim; d++)
    {
        mm[d] = 0;
    }

    /* Initialize lambda point values */
    for(m = 0; m < awh->nlambda; m++)
    {
        for(d = 0; d < awh->ndim; d++)
        {
            lpt[m].lambda[d] = awh->lambda_min[d] + mm[d]*awh->dlambda[d];
            lpt[m].m[d] = mm[d];
        }

        /* For next point, increase index for highest dimension and recursively the lower
           dimension indices that need it */
        step_in_array(awh->ndim,awh->nlambda_dim,mm);
    }

    /* Make sure that the endpoints of the array and the input matches exactly */
    for (d = 0; d < awh->ndim; d++)
    {
        awh->lambda_min[d] = lpt[0].lambda[d];
        awh->lambda_max[d] = lpt[awh->nlambda-1].lambda[d];
    }

    /* For covering checks in initial stage */
    snew(awh->lambda_min_cutoff, awh->ndim);
    snew(awh->lambda_max_cutoff, awh->ndim);

    for (d = 0; d < awh->ndim; d++)
    {
        awh->lambda_min_cutoff[d] = awh->lambda_min[d];
        awh->lambda_max_cutoff[d] = awh->lambda_max[d];
    }

    sfree(mm);
}

static void update_target_and_bias(t_awh *awh)
{
    int m;
    double ln_target, ln_Z;
    t_lambdapoint *lpt = awh->lambdapoint;
    double f_diff_max, f_min, f_max;
    double beta_ratio;

    ln_Z = -GMX_DOUBLE_MAX; /* Z = 0 */

    switch(awh->etarget)
    {
    case eawhtCONSTANT:
    case eawhtUSER_CONSTANT:
        /* Only initialize target */
        if(awh->nsamples == 0)
        {
            for(m = 0; m < awh->nlambda; m++)
            {
                lpt[m].target = lpt[m].ln_target_lambda;
                ln_Z = expsum(ln_Z, lpt[m].target);
            }

            /* Exponentiate and normalize target distribution to 1.
           Also update the biasing weights g = f + ln(target). */
            for(m = 0; m < awh->nlambda; m++)
            {
                ln_target = lpt[m].target - ln_Z;
                lpt[m].bias = lpt[m].f + ln_target;
                lpt[m].target = exp(ln_target);
            }
        }
        else
        {
            for(m = 0; m < awh->nlambda; m++)
            {
                lpt[m].bias = lpt[m].f + log(lpt[m].target);
            }
        }
        break;
    case eawhtCUTOFF:
    case eawhtUSER_CUTOFF:
        f_diff_max = awh->target_param;
        f_min = GMX_DOUBLE_MAX;

        for(m = 0; m < awh->nlambda; m++)
        {
            if(lpt[m].f < f_min)
            {
                f_min = lpt[m].f;
            }
        }

        f_max = f_min + f_diff_max;

        for(m = 0; m < awh->nlambda; m++)
        {
            lpt[m].target = min(0, f_max - lpt[m].f) + lpt[m].ln_target_lambda;
            ln_Z = expsum(ln_Z, lpt[m].target);
        }

        for(m = 0; m < awh->nlambda; m++)
        {
            ln_target = lpt[m].target - ln_Z;
            lpt[m].bias = lpt[m].f + ln_target;
            lpt[m].target = exp(ln_target);
        }

        break;
    case eawhtBOLTZMANN:
        beta_ratio = awh->target_param;

        for(m = 0; m < awh->nlambda; m++)
        {
            lpt[m].target = -beta_ratio * lpt[m].f + lpt[m].ln_target_lambda;
            ln_Z = expsum(ln_Z, lpt[m].target);
        }

        for(m = 0; m < awh->nlambda; m++)
        {
            ln_target = lpt[m].target - ln_Z;
            lpt[m].bias = lpt[m].f + ln_target;
            lpt[m].target = exp(ln_target);
        }

        break;
    }
}

static void init_rcoordbins(t_awh *awh, t_pull *pull)
{
    /* rcoord grid is a refinement of the lambda grid.
       This is the extra points to refine with in each empty interval.
       Currently assumed that the lambda and rcoord grids are the same. */
    const int nextra_points = 0;

    int i,d,dim_index;
    int *ii;
    double *rc_temp;
    int *i_temp;
    t_variance *var_temp;

    /* Set dimensions of histogram */
    snew(awh->nrcoordbin_dim,awh->ndim);

    awh->nrcoordbin = 1;

    for(d = 0; d < awh->ndim; d++)
    {
        awh->nrcoordbin_dim[d] = awh->nlambda_dim[d] + (awh->nlambda_dim[d] - 1)*nextra_points;
        awh->nrcoordbin *= awh->nrcoordbin_dim[d];
    }

    snew(awh->rcoordbin,awh->nrcoordbin);
    snew(awh->drcoord,awh->ndim);

    /* Allocate all bin coordinates and indices in one chunk */
    snew(rc_temp,awh->nrcoordbin*awh->ndim);
    dim_index = 0;
    for(i = 0; i < awh->nrcoordbin; i++)
    {
        awh->rcoordbin[i].rcoord = &(rc_temp[dim_index]);
        dim_index += awh->ndim;
    }

    rc_temp = NULL;

    snew(i_temp,awh->nrcoordbin*awh->ndim);
    dim_index = 0;
    for(i = 0; i < awh->nrcoordbin; i++)
    {
        awh->rcoordbin[i].i = &(i_temp[dim_index]);
        dim_index += awh->ndim;
    }
    i_temp = NULL;

    snew(var_temp,awh->nrcoordbin*awh->ndim);
    dim_index = 0;
    for(i = 0; i < awh->nrcoordbin; i++)
    {
        awh->rcoordbin[i].force_variance = &(var_temp[dim_index]);
        dim_index += awh->ndim;
    }
    var_temp = NULL;

    for(d = 0; d < awh->ndim; d++)
    {
        int c = awh->icoord[d];
        awh->rcoordbin[0].rcoord[d] = awh->lambdapoint[0].lambda[d];
        awh->drcoord[d] = awh->dlambda[d]/(nextra_points + 1);

        /* Since rc >= 0 for pull geometry distance, avoid bins that overlaps < 0 region by shifting
           the  bin by +drcoord (lambda is also assumed to be positive) */
        if (pull->coord[c].eGeom == epullgDIST)
        {
            if(awh->rcoordbin[0].rcoord[d] - 0.5*awh->drcoord[d] < 0)
            {
                awh->rcoordbin[0].rcoord[d] += awh->drcoord[d];
            }
        }
    }

    /* Initialize bin properties.
       Help array keeping track of what lambda index we are at in each dimension */
    snew(ii,awh->ndim);

   /* Start at (0,0,...) */
    for(d = 0; d < awh->ndim; d++)
    {
        ii[d] = 0;
    }

    for(i = 0; i < awh->nrcoordbin; i++)
    {
        for(d = 0; d < awh->ndim; d++)
        {
            awh->rcoordbin[i].rcoord[d] = awh->rcoordbin[0].rcoord[d] + ii[d]*awh->drcoord[d];
            awh->rcoordbin[i].i[d] = ii[d];
            awh->rcoordbin[i].force_variance[d].avg_x= 0;
            awh->rcoordbin[i].force_variance[d].avg_x2= 0;
            awh->rcoordbin[i].force_variance[d].var_x= 0;
            awh->rcoordbin[i].force_variance[d].nx= 0;
        }

        /* Get closest lambda point in lambda point grid */
        get_lambda_index(awh,&(awh->rcoordbin[i].m),awh->rcoordbin[i].rcoord);

        /* Finished with initializing this point */
        step_in_array(awh->ndim,awh->nrcoordbin_dim,ii);
    }

    sfree(ii);
}

static void update_pmf(t_awh *awh, gmx_multisim_t *ms)
{
    int i;
    t_rcoordbin *rcoordbin = awh->rcoordbin;
    double *pmf = awh->pmf;

    /* Need to temporarily exponentiate (~probability) to sum over simulations */
    for(i = 0; i < awh->nrcoordbin; i++)
    {
        pmf[i] = exp(rcoordbin[i].gammas);
    }

    if(awh->nreplicas > 1 && awh->eMultireplica == eawhm_SHARE)
    {
        gmx_sumd_sim(awh->nrcoordbin, pmf, ms);
    }

    /* Take log again to get (non-normalized) PMF */
    for(i = 0; i < awh->nrcoordbin; i++)
    {
        pmf[i] = -log(pmf[i]);
    }
}


static void update_force_stdev(t_awh *awh) /* TODO: multiple sharing replicas? */
{
    int d, i;

    for (i = 0; i < awh->nrcoordbin; i++)
    {
        double var_tot = 0.;
        for (d = 0; d < awh->ndim; d++)
        {
            var_tot += awh->rcoordbin[i].force_variance[d].var_x;
        }

        awh->rcoordbin[i].force_stdev = sqrt(var_tot);
    }
}
static void init_free_energy_vars(t_awh *awh)
{
    int i, m;
    int nsharing_replicas;
    double log_sum, gamma_m, avg;
    t_lambdapoint *lpt = awh->lambdapoint;

    /* Default settings, may change below */
    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].f = 0.;
        lpt[m].ln_target_lambda = 0.;
    }

    /* Keep pmf in gathered array to easily sum over multiple sims
       (really same for visits_tot) */
    /* Note: in principle the number of rcoord bins need not equal
       the number of lambda points, but currently this is awnyway hard-coded */
    snew(awh->pmf,awh->nrcoordbin);

    for (i = 0; i < awh->nrcoordbin; i++)
    {
        awh->pmf[i] = 0;
        awh->rcoordbin[i].gammas = -GMX_DOUBLE_MAX;
        awh->rcoordbin[i].visits_it = 0;
        awh->rcoordbin[i].visits_tot = 0;
    }

    /* Set pmf, f and target to user requested values */
    set_user_vals(awh, awh->bUser_pmf,
                  (awh->etarget == eawhtUSER_CONSTANT || awh->etarget == eawhtUSER_CUTOFF));

    /* Normalize sum f to 0 (not important for updates) */
    avg = 0.;
    for (m = 0; m < awh->nlambda; m++)
    {
        avg += lpt[m].f;
    }
    avg /= awh->nlambda;
    for (m = 0; m < awh->nlambda; m++)
    {
        lpt[m].f -= avg;
    }

    /* Normalize target and apply any cutoffs */
    update_target_and_bias(awh);

    /* Gamma sums need to be consistent with the given pmf.
       The normalization matters because it determines how big effect the next sample has on the pmf profile.
       Should correspond to sampling of N lambdas. Give each replica their share... */
    log_sum = -GMX_DOUBLE_MAX;

    for (m = 0; m < awh->nlambda; m++)
    {
        gamma_m = calc_gamma(awh, awh->rcoordbin[m].rcoord);

        /* sum += lpt[m].target*exp(gamma + awh->pmf[m]) */
        log_sum = expsum(log_sum, log(lpt[m].target) + gamma_m + awh->pmf[m]);
    }

    nsharing_replicas = awh->eMultireplica == eawhm_SHARE ? awh->nreplicas : 1;
    log_sum += log(awh->N*awh->nstsample/(awh->nlambda*nsharing_replicas));
    for (m = 0; m < awh->nlambda; m++)
    {
        awh->rcoordbin[m].gammas = -awh->pmf[m] + log_sum;
    }

    /* Some more lambda point properties. */
    for(m = 0; m < awh->nlambda; m++)
    {
        lpt[m].W_it = 0.;
        lpt[m].W_cover = 0.;
        lpt[m].W_tot= 0.;
        lpt[m].in_scope = TRUE;
    }
}

static void set_user_vals(t_awh *awh, gmx_bool bPmf, gmx_bool bTarget)
{

    int npts, ncols, nvars_min, index, m, d;
    char filename[] = "awh_init.xvg";
    char *fn;
    double **data, *var, tol, dev, val;
    t_lambdapoint *lpt = awh->lambdapoint;

    if (!bPmf && !bTarget)
    {
        return;
    }

    /* For compatibility with how the awh xvg-files are written by g_energy
       it is assumed that the order is: lambda | pmf | f | target|...| */
    fn = gmxlibfn(filename);
    npts = read_xvg(filename, &data, &ncols);

    if (npts != awh->nlambda || npts != awh->nrcoordbin)
    {
        gmx_fatal(FARGS, "In file %s, the number of points (%d) must equal the number of lambda points (%d)"
                  " and the number of reaction coordinate bins (%d)",
                  fn, npts, awh->nlambda, awh->nrcoordbin);
    }

    /* Make sure all lambda points have data, corresponding to the right lambda points */
    if (bTarget && bPmf)
    {
        /* pmf, f, target */
        nvars_min = awh->ndim + 3;
    }
    else if (bTarget && !bPmf)
    {
        /* target */
        nvars_min = awh->ndim + 1;
    }
    else /* pmf, f */
    {
        nvars_min = awh->ndim + 2;
    }

    if (ncols < nvars_min)
    {
        gmx_fatal(FARGS, "Number of columns in file %s (%d) should be at least %d, namely: %s%s%s%s"
                  " (if you only have a PMF you need to calculate/approximate a convolution corresponding to the chosen force constant k).",
                  fn, ncols, nvars_min, "coord|",
                  (bPmf ? "PMF|" : ""), (bPmf ? "conv. PMF|" : ""), (bTarget ? "target" : ""));
    }

    for (m = 0; m < npts; m++)
    {
        /* First comes lambda values */
        for (d = 0; d < awh->ndim; d++)
        {
            tol = 0.1*awh->dlambda[d];
            var = data[d];
            dev = var[m] - lpt[m].lambda[d];

            if(fabs(dev) > tol)
            {
                gmx_fatal(FARGS, "In file %s, point %d column %d with lambda value %g"
                          " deviates from the expected lambda value (%g) with more than "
                          "0.1 of the corresponding lambda spacing (%g)",
                          fn, m, d, var[m], lpt[m].lambda[d], awh->dlambda[d]);
            }
        }
    }

    if (bPmf)
    {
        for (m = 0; m < npts; m++)
        {
            /* Note: strictly speaking the pmf can be calculated knowing f only (and the force constant k).
               It is here assumed the given pmf and f are consistent with each other (as it should be in output
               of a previous awh run or in the user's own calculations */
           awh->pmf[m] = data[awh->ndim][m];
           lpt[m].f = data[awh->ndim + 1][m];
        }
        if (bTarget)
        {
            for (m = 0; m < npts; m++)
            {
                val = data[awh->ndim + 2][m];
                if (val < 0 )
                {
                    gmx_fatal(FARGS, "Target distribution weight at lambda point %d (%g) is negative.", m, val);
                }
                else
                {
                    lpt[m].ln_target_lambda = val > 0 ? log(val) : -GMX_DOUBLE_MAX;
                }
            }
        }
    }
    else if (bTarget)
    {
        for (m = 0; m < npts; m++)
        {
            val = data[awh->ndim][m];
            if (val < 0 )
            {
                gmx_fatal(FARGS, "Target distribution weight at lambda point %d (%g) is negative.", m, val);
            }
            else
            {
                lpt[m].ln_target_lambda = val > 0 ? log(val) : -GMX_DOUBLE_MAX;
            }
        }
    }

    for(m = 0; m < ncols; m++)
    {
        sfree(data[m]);
    }
    sfree(data);
}

/* Only called by master node */
static void update_awhhistory(awhhistory_t *awhhist, t_awh *awh)
{
   int d, m, i;

    awhhist->m = awh->m;
    awhhist->nlambda = awh->nlambda;

    /* If we are not reading an old checkpoint we will have to allocate memory for all arrays */
    if(awhhist->lambdapointhist == NULL)
    {
        snew(awhhist->lambdapointhist,awhhist->nlambda);
    }

    for(m = 0; m < awhhist->nlambda; m++)
    {
        awhhist->lambdapointhist[m].target = awh->lambdapoint[m].target;
        awhhist->lambdapointhist[m].f = awh->lambdapoint[m].f;
        awhhist->lambdapointhist[m].bias = awh->lambdapoint[m].bias;
        awhhist->lambdapointhist[m].W_it = awh->lambdapoint[m].W_it;
        awhhist->lambdapointhist[m].W_cover = awh->lambdapoint[m].W_cover;
        awhhist->lambdapointhist[m].W_tot = awh->lambdapoint[m].W_tot;
    }

    awhhist->nrcoordbin = awh->nrcoordbin;

    if(awhhist->rcoordbinhist == NULL)
    {
        int dim_index;
        t_variance *var_temp;

        snew(awhhist->rcoordbinhist,awhhist->nrcoordbin);

        /* Allocate space for variance struct for each bin */
        snew(var_temp,awhhist->nrcoordbin*awh->ndim);
        dim_index = 0;
        for(i = 0; i < awhhist->nrcoordbin; i++)
        {
            awhhist->rcoordbinhist[i].force_variance = &(var_temp[dim_index]);
            dim_index += awh->ndim;
        }
        var_temp = NULL;
    }

    for(i = 0; i < awhhist->nrcoordbin; i++)
    {
        int d;
        awhhist->rcoordbinhist[i].gammas = awh->rcoordbin[i].gammas;
        awhhist->rcoordbinhist[i].visits_it = awh->rcoordbin[i].visits_it;
        awhhist->rcoordbinhist[i].visits_tot = awh->rcoordbin[i].visits_tot;

        for (d = 0; d < awh->ndim; d++)
        {
            awhhist->rcoordbinhist[i].force_variance[d].avg_x = awh->rcoordbin[i].force_variance[d].avg_x;
            awhhist->rcoordbinhist[i].force_variance[d].avg_x2 = awh->rcoordbin[i].force_variance[d].avg_x2;
            awhhist->rcoordbinhist[i].force_variance[d].var_x = awh->rcoordbin[i].force_variance[d].var_x;
            awhhist->rcoordbinhist[i].force_variance[d].nx = awh->rcoordbin[i].force_variance[d].nx;
        }
    }

    awhhist->nxsteps = awh->nxsteps;
    awhhist->nsamples = awh->nsamples;
    awhhist->in_initial = awh->in_initial;
    awhhist->N = awh->N;
    awhhist->Nmin = awh->Nmin;
    awhhist->flucts_initial = awh->flucts_initial;
    awhhist->nflucts_initial = awh->nflucts_initial;
    awhhist->ndim = awh->ndim;
}

static void restore_awhhistory_from_state(awhhistory_t *awhhist, t_awh *awh, t_pull *pull, t_commrec *cr)
{
    int m,i,d;

    if(awh != NULL)
    {
        if(MASTER(cr))
        {
            awh->m = awhhist->m;

            awh->nlambda = awhhist->nlambda;

            for(m = 0; m < awh->nlambda; m++)
           {
                awh->lambdapoint[m].target = awhhist->lambdapointhist[m].target;
                awh->lambdapoint[m].f = awhhist->lambdapointhist[m].f;
                awh->lambdapoint[m].bias = awhhist->lambdapointhist[m].bias;
                awh->lambdapoint[m].W_it = awhhist->lambdapointhist[m].W_it;
                awh->lambdapoint[m].W_cover = awhhist->lambdapointhist[m].W_cover;
                awh->lambdapoint[m].W_tot = awhhist->lambdapointhist[m].W_tot;
            }

            awh->nrcoordbin = awhhist->nrcoordbin;

            for(i = 0; i < awh->nrcoordbin; i++)
            {
                awh->rcoordbin[i].gammas = awhhist->rcoordbinhist[i].gammas;
                awh->rcoordbin[i].visits_it = awhhist->rcoordbinhist[i].visits_it;
                awh->rcoordbin[i].visits_tot = awhhist->rcoordbinhist[i].visits_tot;

                for (d = 0; d < awh->ndim; d++)
                {
                    awh->rcoordbin[i].force_variance[d].avg_x = awhhist->rcoordbinhist[i].force_variance[d].avg_x;
                    awh->rcoordbin[i].force_variance[d].avg_x2 = awhhist->rcoordbinhist[i].force_variance[d].avg_x2;
                    awh->rcoordbin[i].force_variance[d].var_x = awhhist->rcoordbinhist[i].force_variance[d].var_x;
                    awh->rcoordbin[i].force_variance[d].nx = awhhist->rcoordbinhist[i].force_variance[d].nx;
                }
            }

            awh->nxsteps = awhhist->nxsteps;
            awh->nsamples = awhhist->nsamples;
            awh->flucts_initial = awhhist->flucts_initial;
            awh->nflucts_initial = awhhist->nflucts_initial;
            awh->in_initial = awhhist->in_initial;
            awh->N = awhhist->N;
            awh->Nmin = awhhist->Nmin;
            awh->ndim = awhhist->ndim;

            for(d = 0; d < awh->ndim; d++)
            {
                int c = awh->icoord[d];
                pull->coord[c].rc_ref =  awh->lambdapoint[awh->m].lambda[d];
            }
        }

        if(PAR(cr))
        {
            /* Restore the history dependent data needed by all ranks */
            gmx_bcast(sizeof(awh->nxsteps), &(awh->nxsteps), cr);
        }
    }
    else
    {
        gmx_fatal(FARGS,"Mismatch between input and checkpoint file."
                  "Pulling with awh is active in one file but not the other.");
    }
}
